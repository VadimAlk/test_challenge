# CSV transactions loader. #
This is the test project designed to load information from CSV files, process it and present to user.

### Project's purpose ###
Project's purpose is loading of Customers and Transactions information from corresponding CSV files, match transactions to customers, provide a way for the user to see above-mentioned data and run predefined filters.

### Features: ###
 - Implemented using WinForms as the user interface, no dependencies besides .NET framework,
 - Loaded transactions records are sorted by their time-stamps, can be useful while merging data from multiple streams,
 - Easily extendable to process data from any other sources by using the common interface,
 - Data processing is done in the separate DLL, can be connected to any other frontend if required,
 - Opens files in read-only shared-access mode, so they can stay opened in other applications (i.e. Excel),
 - Detects unknown (unmatched) customers and add them internally to make sure we do not miss any transactions

### Limitations: ###
 - Records are not checked for appearance of internal commas,
 - Bad / corrupted CSV records are skipped from loading,
 - Transactions with the identical time-stamps will be processed in order of appearance in the file,
 - Chain of transactions is not checked for missing data or duplicates,
 - DateTime format is to not currently configurable and must be recorded as 'M/d/yyyy H:mm'

Version 1.0.0.0

## How do I get set up? ##

### Summary of set up ###
 - Pull / Download the repository, compile the project in the Visual Studio (VS 2013 or higher) or other C# compiler of your choice. Run the executable.
 - Select the Customers file path, select Transactions file path
 - Click Load.
 - Information loaded from files (Customers, will be presented in 3 interactive master-details tables and 4 read-only predefined filter tables.

### Configuration ###
 - No specific configuration required

### Dependencies ###
 - Required .NET 4.5 Framework to be installed on the machine.