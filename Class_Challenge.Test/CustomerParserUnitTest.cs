﻿using System;
using Class_Challenge.BaseClasses;
using Class_Challenge.Parsers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Class_Challenge.Test
{
    [TestClass]
    public class CustomerParserUnitTest
    {
        [TestMethod]
        public void CustomerParserSimple()
        {
            CustomerCsvParser parser = new CustomerCsvParser();
            string[] row = new string[] { "1", "Test", "Customer" };
            var customer = parser.Parse(row);

            Assert.IsNotNull(customer);
            Assert.AreEqual(1, customer.ID);
            Assert.AreEqual("Test", customer.FirstName);
            Assert.AreEqual("Customer", customer.LastName);
        }

        [TestMethod]
        public void CustomerParserCorruptedRecords()
        {
            CustomerCsvParser parser = new CustomerCsvParser();
            string[] row = new string[] { "1", "Test" };
            var customer = parser.Parse(row);
            Assert.IsNull(customer);

            row = new string[] { "corrupt", "data", "test" };
            customer = parser.Parse(row);

            Assert.IsNull(customer);
        }
    }
}
