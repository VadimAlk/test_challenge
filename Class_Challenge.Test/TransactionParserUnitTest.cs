﻿using System;
using Class_Challenge.BaseClasses;
using Class_Challenge.Parsers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Class_Challenge.Test
{
    [TestClass]
    public class TransactionParserUnitTest
    {
        [TestMethod]
        public void TransactionParserSimple()
        {
            TransactionCsvParser parser = new TransactionCsvParser();
            DataStore dataStore = new DataStore();
            Customer customer = new Customer(11, "Test", "Test");
            dataStore.AddOrUpdateCustomer(customer);
            string[] row = new string[] { "11", "1", "100", "CIN", "1000.00", @"1/1/2017 10:00" };

            var transaction = parser.Parse(row, dataStore);

            Assert.IsNotNull(transaction);
            Assert.IsNotNull(transaction.Account);
            Assert.AreEqual(1, customer.NumberOfAccounts());
            Assert.AreEqual(100.0M, transaction.Amount);
            Assert.AreEqual(1000.0M, transaction.Balance);
            Assert.AreEqual(TransactionTypes.CashIn, transaction.Code);
        }

        [TestMethod]
        public void TransactionParserCorruptedRecords()
        {
            TransactionCsvParser parser = new TransactionCsvParser();
            DataStore dataStore = new DataStore();
            Customer customer = new Customer(11, "Test", "Test");
            dataStore.AddOrUpdateCustomer(customer);
            string[] row = new string[] { "1", "Test" };
            var transaction = parser.Parse(row, dataStore);
            Assert.IsNull(transaction);

            row = new string[] { "corrupt", "data", "test", "for", "6", "fields" };
            transaction = parser.Parse(row, dataStore);

            Assert.IsNull(transaction);
        }
    }
}
