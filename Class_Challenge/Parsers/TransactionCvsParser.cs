﻿using Class_Challenge.BaseClasses;
using Class_Challenge.Interfaces;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Class_Challenge.Parsers
{
    /// <summary>
    /// Class providing the parsing of csv row into the Transaction object
    /// </summary>
    public class TransactionCsvParser
    {
        /// <summary>
        /// DateTime string format, can be moved to cofiguration later
        /// </summary>
        private string _parsingDateFormat = @"M/d/yyyy H:mm";

        /// <summary>
        /// Parse the row from csv file into the Transaction object
        /// </summary>
        /// <param name="record">array of row values</param>
        /// <param name="dataStore">DataStore to be used for lookup</param>
        /// <returns>Transaction object reconstructed, or null if transfrmation fialed</returns>
        public Transaction Parse(string[] record, DataStore dataStore)
        {
            #region Customer
            if (record.Length != 6)
                return null;

            long id;
            if (!long.TryParse(record[0], out id))
            {
                // Log error
                return null;
            }

            // Get customer from the list
            var customer = dataStore.GetCustomerById(id);
            if (customer == null)
            {
                // Cretae unknown customer as we don't want to lose the transaction information
                customer = new Customer(id, "Unknown", "Customer");
                dataStore.AddOrUpdateCustomer(customer);
            }
            #endregion

            #region Account
            if (!long.TryParse(record[1], out id))
            {
                // Log error
                return null;
            }         
            var account = customer.GetAccountById(id);
            #endregion Account

            #region Amount
            decimal amount = 0.0M;
            if (!decimal.TryParse(record[2], out amount))
            {
                // Log error
                return null;
            }
            #endregion

            #region Code
            TransactionTypes code = TransactionTypes.CashIn;
            if (record[3].Trim().ToUpper() == "COT")
            {
                code = TransactionTypes.CashOut;
            }
            #endregion

            #region Balance
            decimal balance = 0.0M;
            if (!decimal.TryParse(record[4], out balance))
            {
                // Log error
                return null;
            }
            #endregion

            #region Date
            DateTime date;
            if (!DateTime.TryParseExact(record[5], _parsingDateFormat, CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
            {
                // Log error
                return null;
            }
            #endregion

            if (account == null)
            {
                account = new Account(id);
                customer.AddAccount(account);
            }
            
            var transaction = new Transaction(customer, account, amount, code, balance, date);
            account.AddTransaction(transaction);
            return transaction;
        }
    }
}