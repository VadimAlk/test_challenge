﻿using Class_Challenge.BaseClasses;
using Class_Challenge.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Class_Challenge.Parsers
{
    /// <summary>
    /// Class providing the parsing of csv row into the Customer object
    /// </summary>
    public class CustomerCsvParser
    {
        /// <summary>
        /// Parse the row from csv file into the Customer object
        /// </summary>
        /// <param name="record">array of row values</param>
        /// <returns>Customer object reconstructed, or null if transfrmation fialed</returns>
        public Customer Parse(string[] record)
        {
            long customerId;
            if (record.Length != 3)
                return null;

            if (!long.TryParse(record[0], out customerId))
            {
                // Log error
                return null;
            }
            return new Customer(customerId, record[1], record[2]);
        }
    }
}
