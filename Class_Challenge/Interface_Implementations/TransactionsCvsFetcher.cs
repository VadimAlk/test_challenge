﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Class_Challenge.Interfaces;
using Class_Challenge.BaseClasses;
using Class_Challenge.Parsers;
using System.IO;

namespace Class_Challenge.Interfaces
{
    public class TransactionsCvsFetcher : ITransactionsFetcher
    {
        #region private fields
        /// <summary>
        /// Headers we expect to see in the file. For detection of the first row meaning
        /// </summary>
        private readonly string[] _fileHeaders = new string[] { "Customer", "Account", "Amount", "Code", "Balance", "Date" };

        /// <summary>
        /// Number of data fields we expect to see
        /// </summary>
        private readonly int _numberOfDataFields = 6;

        /// <summary>
        /// Filename of the file to read
        /// </summary>
        private readonly string _fileName;

        /// <summary>
        /// Parser to be used to transform external data to object
        /// </summary>
        private TransactionCsvParser Parser;

        private DataStore _dataStore; 
        #endregion


        /// <summary>
        /// Public constructor
        /// </summary>
        /// <param name="fileName"></param>
        public TransactionsCvsFetcher(string fileName, DataStore dataStore)
        {
            _fileName = fileName;
            Parser = new TransactionCsvParser();
            _dataStore = dataStore;
        }

        /// <summary>
        /// ITransactionsFetcher implementation
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Transaction> GetTransactionsFromSource()
        {
            if (!File.Exists(_fileName)) yield break;
            var fs = new FileStream(_fileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite); 
            using (StreamReader sr = new StreamReader(fs))
            {
                string line;
                bool firstRecord = true;
                while ((line = sr.ReadLine()) != null)
                {
                    var record = line.Split(',');
                    if (record.Length != _numberOfDataFields)
                    {
                        // Log error
                    }
                    else
                    {
                        // detect file headers
                        if (firstRecord)
                        {
                            firstRecord = false;
                            bool match = true;
                            for (int i = 0; i < _numberOfDataFields; i++)
                                match &= record[i] == _fileHeaders[i];
                            if (match) continue;
                        }

                        var transaction = Parser.Parse(record, _dataStore);
                        if (transaction != null)
                            yield return transaction;
                    }
                }
            }
        }
    }
}