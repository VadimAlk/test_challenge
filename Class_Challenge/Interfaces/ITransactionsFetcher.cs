﻿using Class_Challenge.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Class_Challenge.Interfaces
{
    /// <summary>
    /// Interface responsible for retrieving transactions information from the data source
    /// </summary>
    public interface ITransactionsFetcher
    {
        /// <summary>
        /// Implements the retrieval of transactions from the data source
        /// </summary>
        /// <returns>Transactions acquired from the source</returns>
        IEnumerable<Transaction> GetTransactionsFromSource();
    }
}
