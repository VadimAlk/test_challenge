﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Class_Challenge.BaseClasses;

namespace Class_Challenge.Interfaces
{
    /// <summary>
    /// Interface responsible for retrieving customer's information from the data source
    /// </summary>
    public interface ICustomerFetcher
    {
        /// <summary>
        /// Implements the retrieval of customer records from the data source
        /// </summary>
        /// <returns>Customer records acquired from the source</returns>
        IEnumerable<Customer> GetCustomersFromSource();
    }
}
