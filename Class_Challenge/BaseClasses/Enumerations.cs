﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Class_Challenge.BaseClasses
{
    public enum TransactionTypes
    {
        CashIn = 0,
        CashOut = 1
    }

    public enum FetchDataSources
    {
        CsvFile = 0
    }
}
