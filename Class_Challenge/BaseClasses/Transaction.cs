﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Class_Challenge.BaseClasses
{
    /// <summary>
    /// Class representing a single financial transaction
    /// </summary>
    public class Transaction
    {
        #region public properties
        /// <summary>
        /// Customer associated with transaction
        /// </summary>
        public Customer Customer { get; private set; }

        /// <summary>
        /// Acocunt ID of the transaction
        /// </summary>
        public Account Account { get; private set; }

        /// <summary>
        /// Transaction amount
        /// </summary>
        public decimal Amount { get; private set; }

        /// <summary>
        /// Transaction type
        /// </summary>
        public TransactionTypes Code { get; private set; }

        /// <summary>
        /// Remaining balance after the transaction
        /// </summary>
        public decimal Balance { get; private set; }

        /// <summary>
        /// Timestamp of the transaction
        /// </summary>
        public DateTime Timestamp { get; private set; }
        #endregion

        /// <summary>
        /// Class constructor
        /// </summary>
        /// <param name="customer">Customer associated with transaction</param>
        /// <param name="accountID">Acocunt ID of the transaction</param>
        /// <param name="amount">Transaction amount</param>
        /// <param name="code">Transaction type</param>
        /// <param name="balance">Remaining balance after the transaction</param>
        /// <param name="timestamp">Timestamp of the transaction</param>
        public Transaction(Customer customer, Account account, decimal amount, TransactionTypes code, decimal balance, DateTime timestamp)
        {
            Customer = customer;
            Account = account;
            Amount = amount;
            Code = code;
            Balance = balance;
            Timestamp = timestamp;
        }
    }

}


