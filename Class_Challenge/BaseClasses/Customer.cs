﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Class_Challenge.BaseClasses
{
    /// <summary>
    /// Class representing a customer information
    /// </summary>
    public class Customer
    {
        /// <summary>
        /// List of customer's accounts
        /// </summary>
        private Dictionary<long, Account> _accounts;

        #region public properties
        /// <summary>
        /// ID of the customer
        /// </summary>
        public long ID { get; private set; }

        /// <summary>
        /// Customer's first name
        /// </summary>
        public string FirstName { get; private set; }

        /// <summary>
        /// Customer's last name
        /// </summary>
        public string LastName { get; private set; }
        #endregion

        /// <summary>
        /// Class constructor
        /// </summary>
        /// <param name="id">Customer's ID</param>
        /// <param name="firstName">First name of the customer</param>
        /// <param name="lastName">Last name of the customer</param>
        public Customer(long id, string firstName, string lastName)
        {
            ID = id;
            FirstName = firstName;
            LastName = lastName;
            _accounts = new Dictionary<long, Account>();
        }

        #region public methods
        /// <summary>
        /// Change name of the customer
        /// </summary>
        /// <param name="newFirstName">New first name</param>
        /// <param name="newLastName">New last name</param>
        public void ChangeCustomemersName(string newFirstName, string newLastName)
        {
            FirstName = newFirstName;
            LastName = newLastName;
        }

        /// <summary>
        /// Add the account to the customer
        /// </summary>
        /// <param name="account">Account to be added</param>
        public void AddAccount(Account account)
        {
            if (account != null)
                _accounts[account.ID] = account;
        }

        /// <summary>
        /// Remove the account from the list
        /// </summary>
        /// <param name="account">Account to be removed</param>
        public void RemoveAccount(Account account)
        {
            if (account != null && _accounts.ContainsKey(account.ID))
                _accounts.Remove(account.ID);
        }

        /// <summary>
        /// Returns customers account with specified ID
        /// </summary>
        /// <param name="id">Accounts's ID</param>
        /// <returns>customers account with specified ID</returns>
        public Account GetAccountById(long id)
        {
            if (_accounts.ContainsKey(id))
                return _accounts[id];
            else return null;
        }

        /// <summary>
        /// Get all accounts of the customer
        /// </summary>
        /// <returns>All accounts of the customer</returns>
        public List<Account> GetAllAccounts()
        {
            return _accounts.Values.ToList();
        }

        /// <summary>
        /// Get total number of customer's accounts
        /// </summary>
        /// <returns>Total number of customer's accounts</returns>
        public int NumberOfAccounts()
        {
            return _accounts.Count;
        }

        /// <summary>
        /// Get total balance of customer's accounts
        /// </summary>
        /// <returns>Total balance of customer's accounts</returns>
        public decimal TotalBalance()
        {
            return _accounts.Sum(x => x.Value.AccountBalance());
        }

        /// <summary>
        /// Overridden ToString() implementation
        /// </summary>
        /// <returns>Human-readable class presentation</returns>
        public override string ToString()
        {
            return string.Format("{0} {1} (ID: {2})", FirstName, LastName, ID);
        }
        #endregion
    }
}
