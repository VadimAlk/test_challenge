﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Class_Challenge.BaseClasses
{
    /// <summary>
    /// Central data storage place for lookup and data exchange
    /// </summary>
    public class DataStore
    {
        #region private fields
        /// <summary>
        /// Stores the list of all known customers
        /// </summary>
        private Dictionary<long, Customer> _customers = new Dictionary<long,Customer>();

        /// <summary>
        /// Stores the list of known accounts
        /// </summary>
        private Dictionary<long, Account> _accounts = new Dictionary<long,Account>();
        #endregion

        #region public functions
        /// <summary>
        /// Add or update the existing customer based of customer's ID
        /// </summary>
        /// <param name="customer">Customer to be added</param>
        public void AddOrUpdateCustomer(Customer customer)
        {
            if (customer != null) _customers[customer.ID] = customer;
        }

        /// <summary>
        /// Get the list of all known customers
        /// </summary>
        /// <returns>list of all known customers</returns>
        public List<Customer> GetAllCustomers()
        {
            return _customers.Values.ToList();
        }

        /// <summary>
        /// Get the customer by ID
        /// </summary>
        /// <param name="id">Customers ID</param>
        /// <returns>Customer found or null otherwise</returns>
        public Customer GetCustomerById(long id)
        {
            if (_customers.ContainsKey(id))
                return _customers[id];
            else
                return null;
        }

        /// <summary>
        /// Get account by account ID
        /// </summary>
        /// <param name="id">Account ID</param>
        /// <returns>Account with requested ID</returns>
        public Account GetAccountById(long id)
        {
            if (_accounts.ContainsKey(id))
                return _accounts[id];
            else
            {
                Account account = new Account(id);
                _accounts[id] = account;
                return account;
            }
        }
        #endregion
    }
}
