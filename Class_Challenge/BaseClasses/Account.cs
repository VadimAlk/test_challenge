﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Class_Challenge.BaseClasses
{
    // Design considerations
    // This class has being optimised in the assumption that transactions list is
    // loaded ones, possibly in one go, and then accessed frequently.
    // Also:
    // 1. The list of transactions can arrive not sorted by time (for instance, merged from several sources)
    // 2. Transactions with the same timestamp will be considered in the order of the addition

    /// <summary>
    /// Class representing a single account
    /// </summary>
    public class Account
    {
        #region private fields
        /// <summary>
        /// Time-Sorted list of transactions
        /// </summary>
        private SortedList<DateTime, Transaction> _transactions;

        /// <summary>
        /// Last known time-wise transaction. Used to provide account balance
        /// </summary>
        private Transaction _lastTransaction;
        #endregion

        /// <summary>
        /// Account ID
        /// </summary>
        public long ID { get; private set; }

        /// <summary>
        /// Public constructor
        /// </summary>
        /// <param name="id">Account ID</param>
        public Account(long id)
        {
            ID = id;
            _transactions = new SortedList<DateTime, Transaction>(1024);
            _lastTransaction = null;
        }

        #region public methods
        /// <summary>
        /// Add a single transaction to the account
        /// </summary>
        /// <param name="transaction">Transaction to be added</param>
        public void AddTransaction(Transaction transaction)
        {
            _transactions.Add(transaction.Timestamp, transaction);
            if (_lastTransaction == null || _lastTransaction.Timestamp <= transaction.Timestamp)
            {
                _lastTransaction = transaction;
            }
        }

        /// <summary>
        /// Add the range of transactions
        /// </summary>
        /// <param name="range">Range of transactions to be added</param>
        public void AddRangeOfTransactions(IEnumerable<Transaction> range)
        {
            foreach (var transacation in range)
            {
                AddTransaction(transacation);
            }
        }

        /// <summary>
        /// Last known ballance of the account 
        /// </summary>
        /// <returns>Last known ballance of the account</returns>
        public decimal AccountBalance()
        {
            if (_lastTransaction == null) return 0.0M;
            return _lastTransaction.Balance;
        }

        /// <summary>
        /// Get all transactions of the account
        /// </summary>
        /// <returns>List of all known transactions for the account</returns>
        public Transaction[] GetTransactions()
        {
            return _transactions.Values.ToArray();
        }

        /// <summary>
        /// Human-readable string representation of the object
        /// </summary>
        /// <returns>Human-readable string representation of the object</returns>
        public override string ToString()
        {
            return string.Format("ID: {0}, Balance: {1}", ID, AccountBalance());
        }
        #endregion
    }
}
