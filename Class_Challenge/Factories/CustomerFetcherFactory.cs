﻿using System;
using Class_Challenge.Interfaces;
using Class_Challenge.BaseClasses;

namespace Class_Challenge.Factories
{
    public class CustomerFetcherFactory
    {
        public static ICustomerFetcher GetCustomerFetcher(FetchDataSources sourceType, string sourceURL)
        {
            switch (sourceType)
            {
                case FetchDataSources.CsvFile: return new CustomerCvsFetcher(sourceURL);
                // case: any other data sources
                default: return new CustomerCvsFetcher(sourceURL);
            }
        }
    }
}
