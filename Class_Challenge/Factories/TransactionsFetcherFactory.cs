﻿using System;
using Class_Challenge.Interfaces;
using Class_Challenge.BaseClasses;

namespace Class_Challenge.Factories
{
    public class TransactionsFetcherFactory
    {
        public static ITransactionsFetcher GetTransactionFetcher(FetchDataSources sourceType, string sourceURL, DataStore dataStore)
        {
            switch (sourceType)
            {
                case FetchDataSources.CsvFile: return new TransactionsCvsFetcher(sourceURL, dataStore);
                // case: any other data sources
                default: return new TransactionsCvsFetcher(sourceURL, dataStore);
            }
        }
    }
}
