﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Class_Challenge.BaseClasses;
using Class_Challenge.Factories;
using Class_UI.Models;

namespace Class_Challenge
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void butLoad_Click(object sender, EventArgs e)
        {
            if (!File.Exists(tbCustomers.Text))
            {
                MessageBox.Show("Customers file is not found");
                return;
            }

            if (!File.Exists(tbTransactions.Text))
            {
                MessageBox.Show("Transactions file is not found");
                return;
            }

            // Load data from files
            DataStore dataStore = new DataStore();
            var customerFetcher = CustomerFetcherFactory.GetCustomerFetcher(FetchDataSources.CsvFile, tbCustomers.Text);
            List<Customer> customers = new List<Customer>(customerFetcher.GetCustomersFromSource());
            
            // Add known customers to DataStore for transaction lookup
            foreach(var customer in customers)
            {
                dataStore.AddOrUpdateCustomer(customer);
            }

            var tranactionsFetcher = TransactionsFetcherFactory.GetTransactionFetcher(FetchDataSources.CsvFile, tbTransactions.Text, dataStore);
            List<Transaction> transactions = new List<Transaction>(tranactionsFetcher.GetTransactionsFromSource());

            // we might get some unknown transaction records, need to update customers list
            customers = dataStore.GetAllCustomers();

            // Prepare data presentation
            CreateModelsAndBind(customers);

            PopulateFilters(customers, transactions);
        }

        /// <summary>
        /// Populate custom filers required
        /// </summary>
        /// <param name="customers">List of customers</param>
        /// <param name="transactions">List of transactions</param>
        private void PopulateFilters(List<Customer> customers, List<Transaction> transactions)
        {
            dgvFilter1.DataSource = customers.Where(x => x.NumberOfAccounts() == 0).ToList();
            dgvFilter2.DataSource = customers.Where(x => x.NumberOfAccounts() > 1).ToList();
            dgvFilter3.DataSource = transactions.Where(x => x.Timestamp.Hour >= 18 || x.Timestamp.Hour < 6).ToList();
            dgvFilter4.DataSource = transactions.Where(x => x.Code == TransactionTypes.CashOut && x.Amount > x.Balance).ToList();
        }

        /// <summary>
        /// Creates presentation models for customers list
        /// </summary>
        /// <param name="customers">List of customers</param>
        private void CreateModelsAndBind(List<Customer> customers)
        {
            List<CustomerModel> customerModels = new List<CustomerModel>();
            foreach (var customer in customers)
                customerModels.Add(new CustomerModel(customer));
            dgvCustomers.DataSource = customerModels;
        }

        private void dgvCustomers_SelectionChanged(object sender, EventArgs e)
        {
            dgvAccounts.DataSource = null;
            if (dgvCustomers.SelectedRows.Count > 0)
            {
                dgvAccounts.DataSource = ((CustomerModel)dgvCustomers.SelectedRows[0].DataBoundItem).CustomerAccounts;
            }
        }

        private void dgvAccounts_SelectionChanged(object sender, EventArgs e)
        {
            dgvTransactions.DataSource = null;
            if (dgvAccounts.SelectedRows.Count > 0)
            {
                dgvTransactions.DataSource = ((AccountModel)dgvAccounts.SelectedRows[0].DataBoundItem).Transactions;
            }
        }
    }
}
