﻿using Class_Challenge.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Class_UI.Models
{
    /// <summary>
    /// Representation model of Transaction object
    /// </summary>
    public class TransactionModel
    {
        /// <summary>
        /// Transaction to be presented
        /// </summary>
        private Transaction _transaction;

        /// <summary>
        /// Transaction amount
        /// </summary>
        public string Amount { get { return _transaction.Amount.ToString("N2"); } }

        /// <summary>
        /// Transaction code
        /// </summary>
        public string Code { get { return _transaction.Code == TransactionTypes.CashIn ? "CIN" : "COT"; } }

        /// <summary>
        /// Transaction balance after transaction happened
        /// </summary>
        public string Balance { get { return _transaction.Balance.ToString("N2"); } }

        /// <summary>
        /// Transaction's timestamp
        /// </summary>
        public string Date { get { return _transaction.Timestamp.ToString(); } }

        public TransactionModel(Transaction transaction)
        {
            _transaction = transaction;
        }
    }
}
