﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Class_Challenge.BaseClasses;

namespace Class_UI.Models
{
    /// <summary>
    /// Presentation model of customer object
    /// </summary>
    class CustomerModel
    {
        /// <summary>
        /// Customer to be presented
        /// </summary>
        private readonly Customer _customer;

        /// <summary>
        /// Customer's ID
        /// </summary>
        public string ID { get { return _customer.ID >= 0 ? _customer.ID.ToString() : "N/A"; } }

        /// <summary>
        /// Customer's first name
        /// </summary>
        public string Name { get { return _customer.FirstName; } }

        /// <summary>
        /// Customer's last name
        /// </summary>
        public string Surname { get { return _customer.LastName; } }

        /// <summary>
        /// Number of accounts customer owns
        /// </summary>
        public string Accounts { get { return _customer.NumberOfAccounts().ToString(); } }

        /// <summary>
        /// Last known aggregated ballance of all accounts
        /// </summary>
        public string Balance { get { return _customer.TotalBalance().ToString("N2"); } }

        /// <summary>
        /// List of all accounts represneted by AccountModel
        /// </summary>
        public List<AccountModel> CustomerAccounts { get { return GetAccountModels(_customer.GetAllAccounts()); } }

        public CustomerModel(Customer customer)
        {
            _customer = customer;
        }

        /// <summary>
        /// Converts the list of accounts to the list of account models
        /// </summary>
        /// <param name="accounts">list of accounts</param>
        /// <returns>list of account models</returns>
        private List<AccountModel> GetAccountModels(List<Account> accounts)
        {
            List<AccountModel> result = new List<AccountModel>(accounts.Count);
            foreach(var account in accounts)
                result.Add(new AccountModel(account));
            return result;
        }
    }
}
