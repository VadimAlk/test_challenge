﻿using Class_Challenge.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Class_UI.Models
{
    /// <summary>
    /// Representation model of Account object
    /// </summary>
    public class AccountModel
    {
        /// <summary>
        /// Account to be presented
        /// </summary>
        private Account _account;

        /// <summary>
        /// Account ID
        /// </summary>
        public string ID { get { return _account.ID.ToString(); } }

        /// <summary>
        /// Account's last known balance
        /// </summary>
        public string Balance { get { return _account.AccountBalance().ToString("N2"); } }

        /// <summary>
        /// List of account's transactions in form of transaction models
        /// </summary>
        public List<TransactionModel> Transactions { get { return GetTransactionModels(_account.GetTransactions()); } }

        public AccountModel(Account account)
        {
            _account = account;
        }

        /// <summary>
        /// Converts the array of transactions to the list of transaction models
        /// </summary>
        /// <param name="list">array of transactions</param>
        /// <returns>list of transaction models</returns>
        private List<TransactionModel> GetTransactionModels(Transaction[] list)
        {
            List<TransactionModel> result = new List<TransactionModel>(list.Length);
            foreach (var item in list)
                result.Add(new TransactionModel(item));
            return result;
        }

    }
}
